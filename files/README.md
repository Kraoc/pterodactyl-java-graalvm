You need to symlink GraalVM archives from **../graalvm/** folder.

```
ln -s ../graalvm/graalvm-ee-java17-linux-aarch64-22.2.0.tar.gz graalvm.tgz
ln -s ../graalvm/native-image-installable-svm-svmee-java17-linux-aarch64-22.2.0.jar native-image.jar
```


You may adapt filename if using a different version.
