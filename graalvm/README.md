Place here the following files:

- graalvm-ee-java17-linux-aarch64-22.2.0.tar.gz
- native-image-installable-svm-svmee-java17-linux-aarch64-22.2.0.jar

This is to build **GraalVM 22.2.0**.

You need to signin into Oracle website to be able to download archives:

https://www.oracle.com/downloads/graalvm-downloads.html


Once logged, select:
- Java version: 17
- OS: linux
- Architecture: aarch64

Then you download and put here the following files:
- Oracle GraalVM Enterprise Edition JDK
- Oracle GraalVM Enterprise Edition Native Image
